# Stash Keyboard Shortcuts
Adds the keyboard shortcuts that are missing in Stash. Never let your hands
leave the keyboard!

## Available Shortcuts

### Repository Browsing Page
* `c`: Open the clone URL window so you can copy the clone URL.
